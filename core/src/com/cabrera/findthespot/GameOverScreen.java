package com.cabrera.findthespot;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class GameOverScreen implements Screen {

    final FindTheSpot game;
    private Stage stage;
    private Preferences prefs;
    private int score;

    OrthographicCamera camera;

    public GameOverScreen(final FindTheSpot gam, int score) {
        game = gam;

        game.adHandler.showAds(true);

        this.score = score;

        prefs = Gdx.app.getPreferences("Preferences");

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 480, 800);

        stage = new Stage();
        stage.setViewport(new StretchViewport(480, 800));
        Gdx.input.setInputProcessor(stage);

        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        final TextButton textButton=new TextButton("REPLAY", skin, "default");
        textButton.setWidth(300);
        textButton.setHeight(50);
        textButton.setPosition((480-300)/2, 200);
        stage.addActor(textButton);

        textButton.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                game.setScreen( new GameLevelScreen(game));
                dispose();
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.53f, 0.81f, 0.92f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();

        game.batch.begin();
        if(score >= 10) {
            game.fontScore.draw(game.batch, "" + score, 175, 600);
        } else {
            game.fontScore.draw(game.batch, "" + score, 210, 600);
        }
        game.fontHighScoreLabel.draw(game.batch, "High Score ", 140, 420);
        game.fontHighScore.draw(game.batch, "" + prefs.getInteger("highscore"), 295, 428);

        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        Gdx.input.setInputProcessor(null);
    }
}