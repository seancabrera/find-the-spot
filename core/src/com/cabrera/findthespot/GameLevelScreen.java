package com.cabrera.findthespot;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sean on 1/4/2015.
 */
public class GameLevelScreen implements Screen {
    FindTheSpot game;

    Preferences prefs;

    Texture redspotImage;
    Texture greenspotImage;
    Texture clockImage;
    Texture clockTouchedImage;
    Texture bombImage;
    Texture bombGlowImage;
    Texture backgroundImage;
    Texture backgroundBombImage;
    Texture red;

    Sound beep;
    Sound explosion;
    Sound powerup;
    Sound spotFoundSound;
    Music gameMusic;

    OrthographicCamera camera;
    Rectangle redspot;
    List<Rectangle> redspots = new ArrayList<Rectangle>();
    Rectangle greenspot;
    Rectangle targetspot;

    private List<BombState> bombs = new ArrayList<BombState>();
    private List<ClockState> clocks = new ArrayList<ClockState>();

    long lastFoundTime = 0;
    long startTime;
    int score;
    int frozenTime = 0;
    int timeRemaining = 60;
    int SPOT_RADIUS = 48;
    int BOMB_RADIUS = 64;
    long bombExplodedTimeout = 3000000000L;
    long clockTouchedTimeout = 2000000000L;

    boolean bombBackground = false;

    protected GameLevelScreen(FindTheSpot game) {
        this.game = game;

        game.adHandler.showAds(false);

        prefs = Gdx.app.getPreferences("Preferences");

        redspotImage = new Texture(Gdx.files.internal("redspot.png"));
        greenspotImage = new Texture(Gdx.files.internal("greenspot.png"));
        clockImage = new Texture(Gdx.files.internal("clock.png"));
        clockTouchedImage = new Texture(Gdx.files.internal("clock_inverted.png"));
        bombImage = new Texture(Gdx.files.internal("bomb.png"));
        bombGlowImage = new Texture(Gdx.files.internal("exploded_bomb.png"));
        backgroundImage = new Texture(Gdx.files.internal("background.jpg"));
        backgroundBombImage = new Texture(Gdx.files.internal("background_glow.png"));
        red = new Texture(Gdx.files.internal("redspot2.png"));

        clockImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        clockTouchedImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        redspotImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        greenspotImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bombImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bombGlowImage.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        beep = Gdx.audio.newSound(Gdx.files.internal("beep.wav"));
        spotFoundSound = Gdx.audio.newSound(Gdx.files.internal("coin_up.wav"));
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        powerup = Gdx.audio.newSound(Gdx.files.internal("powerup.wav"));
        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("loop.mp3"));
        gameMusic.setLooping(true);

        // create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 480, 800);

        redspot = new Rectangle();
        redspot.x = 800 / 2 - 64 / 2;
        redspot.y = 20;
        // the bottom screen edge
        redspot.width = SPOT_RADIUS;
        redspot.height = SPOT_RADIUS;

        placeRandomTargetSpot();

        generateBombs();

        generateClocks();

        startTime = TimeUtils.nanoTime();

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if(frozenTime == 0) {
                    decrementClock();
                } else {
                    frozenTime--;
                    if(frozenTime == 0) {
                        decrementClock();
                    }
                }
            }
        }, 1, 1);
    }

    private void decrementClock() {
        timeRemaining--;
        if(timeRemaining <= 3) {
            beep.play();
        }
    }

    private void executeBombLogic() {
        game.batch.begin();

        long currentTime = TimeUtils.nanoTime();

        List<BombState> bombsToRemove = new ArrayList<BombState>();

        for (BombState bomb : bombs) {
            // place bomb when the bomb level time hits
            if(bomb.bombLevelTime == timeRemaining && !bomb.bombPlaced) {
                bomb.placeBomb();
                bomb.bombPlaced = true;
            }

            // remove the bomb after 5 seconds
            if(bomb.bombLevelTime - 5 == timeRemaining && !bomb.bombExploded) {
                bomb.bombRect = null;
                bombsToRemove.add(bomb);
            }

            if(bomb.bombRect != null) {
                // dont let a bomb cover the target
                while(bomb.bombRect.overlaps(targetspot)) {
                    bomb.placeBomb();
                }

                if(bomb.bombExploded) {
                    game.batch.draw(bombGlowImage, bomb.bombRect.x-16, bomb.bombRect.y-16);
                } else {
                    game.batch.draw(bombImage, bomb.bombRect.x, bomb.bombRect.y);
                }

                if(Gdx.input.isTouched() && bomb.bombRect.overlaps(redspot) && !bomb.bombExploded) {
                    // the bomb has been touched. change state to exploded
                    bomb.bombExplodedStartTime = TimeUtils.nanoTime();
                    bomb.bombExploded = true;
                    explosion.play();
                    redspots.clear();
                    redspot.x = -100;
                    redspot.y = -100;
                }
            }

            if (bomb.bombExploded) {
                if((bomb.bombExplodedStartTime + bombExplodedTimeout) > currentTime) {
                    bombBackground = true;
                } else {
                    bombBackground = false;
                    bombsToRemove.add(bomb);
                }
            }
        }

        bombs.removeAll(bombsToRemove);

        bombBackground = false;
        for (BombState bomb : bombs) {
            if(bomb.bombExploded) {
                bombBackground = true;
                break;
            }
        }


        game.batch.end();
    }

    private void executeClockLogic() {
        game.batch.begin();

        long currentTime = TimeUtils.nanoTime();

        List<ClockState> clocksToRemove = new ArrayList<ClockState>();

        for (ClockState clock : clocks) {
            // place clock when the bomb level time hits
            if(clock.clockLevelTime == timeRemaining && !clock.clockPlaced) {
                boolean clockPlacementGood = false;
                while(!clockPlacementGood) {
                    clockPlacementGood = true;
                    clock.placeClock();
                    for (BombState bomb : bombs) {
                        if(bomb.bombRect != null && clock.clockRect.overlaps(bomb.bombRect)) {
                            clockPlacementGood = false;
                            break;
                        }
                    }
                }
                clock.clockPlaced = true;
            }

            // remove the clock after 1 seconds
            if(clock.clockLevelTime - 1 == timeRemaining && !clock.clockTouched) {
                clock.clockRect = null;
                clocksToRemove.add(clock);
            }

            if(clock.clockRect != null) {
                if(clock.clockTouched) {
                    // TODO change the image
                    game.batch.draw(clockTouchedImage, clock.clockRect.x, clock.clockRect.y);
                } else {
                    game.batch.draw(clockImage, clock.clockRect.x, clock.clockRect.y);
                }

                Vector3 inputVector = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(inputVector);
                if(Gdx.input.isTouched() && (clock.clockRect.overlaps(redspot) || clock.clockRect.contains(inputVector.x, inputVector.y))  && !clock.clockTouched) {
                    // the clock has been touched. change state to touched
                    clock.clockTouchedStartTime = TimeUtils.nanoTime();
                    clock.clockTouched = true;
                    powerup.play();
                    frozenTime += 6;    // 5 + 1 (offset)
                }
            }

            if (clock.clockTouched) {
                if((clock.clockTouchedStartTime + clockTouchedTimeout) > currentTime) {
                } else {
                    clocksToRemove.add(clock);
                }
            }
        }

        clocks.removeAll(clocksToRemove);

        game.batch.end();
    }

    private void generateBombs() {
        // Randomly time the bombs but make it progressively harder
        for (int i=0; i<30; i++) {
            BombState bomb = new BombState(MathUtils.random(0, 45));
            bombs.add(bomb);
        }
        for (int i=0; i<10; i++) {
            BombState bomb = new BombState(MathUtils.random(0, 30));
            bombs.add(bomb);
        }
        for (int i=0; i<10; i++) {
            BombState bomb = new BombState(MathUtils.random(0, 10));
            bombs.add(bomb);
        }
    }

    private void generateClocks() {
        for (int i=0; i<2; i++) {
            ClockState clock = new ClockState(MathUtils.random(0, 45));
            clocks.add(clock);
        }
    }

    protected void placeRandomTargetSpot() {
        targetspot = new Rectangle();
        targetspot.x = MathUtils.random(0, 480 - SPOT_RADIUS);
        targetspot.y = MathUtils.random(0, 800 - SPOT_RADIUS);
        targetspot.width = SPOT_RADIUS;
        targetspot.height = SPOT_RADIUS;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.53f, 0.81f, 0.92f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // This is another way to fix the font on tablets
//        Gdx.gl.glTexParameteri(Gdx.gl.GL_TEXTURE_2D, Gdx.gl.GL_TEXTURE_MAG_FILTER, Gdx.gl.GL_LINEAR);
//        Gdx.gl.glTexParameteri(Gdx.gl.GL_TEXTURE_2D, Gdx.gl.GL_TEXTURE_MIN_FILTER, Gdx.gl.GL_LINEAR);

        // tell the camera to update its matrices.
        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();

        if(bombBackground) {
            game.batch.draw(red, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        } else {
//            game.batch.draw(backgroundImage, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }

        if(timeRemaining <= 0) {
            // the contains doesn't seem necessary but it's safer to keep it
            if(!prefs.contains("highscore") || score > prefs.getInteger("highscore")) {
                prefs.putInteger("highscore", score);
                prefs.flush();
            }

            dispose();
            Screen nextScreen = new GameOverScreen(game, score);
            game.setScreen(nextScreen);
        }

        for (Rectangle redspot : redspots) {
            game.batch.draw(redspotImage, redspot.x, redspot.y);
        }

        if(TimeUtils.nanoTime() - lastFoundTime < 1000000000) {
            game.batch.draw(greenspotImage, greenspot.x, greenspot.y);
        }

        game.batch.end();

        // process user input
        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);

            if (!bombBackground) {
                redspot.x = touchPos.x - SPOT_RADIUS/2;
                redspot.y = touchPos.y - SPOT_RADIUS/2;

                Rectangle rectangle = new Rectangle();
                rectangle.x = redspot.x;
                rectangle.y = redspot.y;
                rectangle.width = SPOT_RADIUS;
                rectangle.height = SPOT_RADIUS;
                redspots.add(rectangle);
            }

            if(targetspot.overlaps(redspot)) {
                spotFoundSound.play();

                lastFoundTime = TimeUtils.nanoTime();
                score++;
                redspots.clear();


                greenspot = new Rectangle();
                greenspot.x = targetspot.x;
                greenspot.y = targetspot.y;
                greenspot.width = targetspot.width;
                greenspot.height = targetspot.height;

                placeRandomTargetSpot();
            }
        }

        executeBombLogic();
        executeClockLogic();

        game.batch.begin();

        game.font.draw(game.batch, "" + score, 25, 800 - 3);

        BitmapFont timerFont;
        if(frozenTime > 0) {
            timerFont = game.fontShadow;
        } else {
            timerFont = game.font;
        }
        if(timeRemaining >= 10) {
            timerFont.draw(game.batch, "" + timeRemaining, 407, 800 - 3);
        } else {
            timerFont.draw(game.batch, "" + timeRemaining, 430, 800 - 3);
        }
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        gameMusic.play();
    }

    @Override
    public void hide() {
        gameMusic.stop();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        // dispose all sounds
        beep.dispose();
        explosion.dispose();
        powerup.dispose();
        spotFoundSound.dispose();
        gameMusic.dispose();

        // dispose all textures
        redspotImage.dispose();
        greenspotImage.dispose();
        clockImage.dispose();
        clockTouchedImage.dispose();
        bombImage.dispose();
        bombGlowImage.dispose();
        backgroundImage.dispose();
        backgroundBombImage.dispose();
        red.dispose();
    }


    private class BombState {
        public int bombLevelTime;
        public long bombExplodedStartTime;
        public Rectangle bombRect;
        public boolean bombPlaced;
        public boolean bombExploded;

        public BombState(int bombLevelTime) {
            this.bombLevelTime = bombLevelTime;
        }

        public void placeBomb() {
            bombRect = new Rectangle();
            bombRect.x = MathUtils.random(0, 480 - BOMB_RADIUS);
            bombRect.y = MathUtils.random(0, 800 - BOMB_RADIUS - 50);
            bombRect.width = BOMB_RADIUS;
            bombRect.height = BOMB_RADIUS;
        }
    }

    private class ClockState {
        public int clockLevelTime;
        public long clockTouchedStartTime;
        public Rectangle clockRect;
        public boolean clockPlaced;
        public boolean clockTouched;

        public ClockState(int clockLevelTime) {
            this.clockLevelTime = clockLevelTime;
        }

        public void placeClock() {
            clockRect = new Rectangle();
            clockRect.x = MathUtils.random(0, 480 - BOMB_RADIUS);
            clockRect.y = MathUtils.random(0, 800 - BOMB_RADIUS - 50);
            clockRect.width = BOMB_RADIUS;
            clockRect.height = BOMB_RADIUS;
        }
    }
}
