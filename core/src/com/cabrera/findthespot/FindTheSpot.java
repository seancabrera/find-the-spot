package com.cabrera.findthespot;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FindTheSpot extends Game {

    IAdHandler adHandler;
    SpriteBatch batch;
    BitmapFont font;
    BitmapFont fontShadow;
    BitmapFont fontScore;
    BitmapFont fontHighScoreLabel;
    BitmapFont fontHighScore;

    public FindTheSpot(IAdHandler adHandler) {
        this.adHandler = adHandler;
    }

    public void create() {
        batch = new SpriteBatch();
        // Use Font made by hiero jar
        font = new BitmapFont(Gdx.files.internal("Arial.fnt"), Gdx.files.internal("Arial.png"), false);
        fontShadow = new BitmapFont(Gdx.files.internal("Arial_shadow.fnt"), Gdx.files.internal("Arial_shadow.png"), false);
        fontScore = new BitmapFont(Gdx.files.internal("score.fnt"), Gdx.files.internal("score.png"), false);
        fontHighScoreLabel = new BitmapFont(Gdx.files.internal("highscorelabel.fnt"), Gdx.files.internal("highscorelabel.png"), false);
        fontHighScore = new BitmapFont(Gdx.files.internal("highscore.fnt"), Gdx.files.internal("highscore.png"), false);
        fontHighScore.setScale(0.5f);

        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontShadow.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontHighScore.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontHighScoreLabel.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontScore.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
        super.render(); // important!
    }

    public void dispose() {
        batch.dispose();
        font.dispose();
        fontShadow.dispose();
        fontScore.dispose();
        fontHighScoreLabel.dispose();
        fontHighScore.dispose();
    }

}