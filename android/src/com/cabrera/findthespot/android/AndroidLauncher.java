package com.cabrera.findthespot.android;

import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.cabrera.findthespot.FindTheSpot;
import com.cabrera.findthespot.IAdHandler;

public class AndroidLauncher extends AndroidApplication implements IAdHandler {

    AdView mAdView;


    // This handler COULD be used..
//    private final int SHOW_ADS = 1;
//    private final int HIDE_ADS = 0;

//    protected Handler handler = new Handler()
//    {
//        @Override
//        public void handleMessage(Message msg) {
//            switch(msg.what) {
//                case SHOW_ADS:
//                {
//                    mAdView.setVisibility(View.VISIBLE);
//                    break;
//                }
//                case HIDE_ADS:
//                {
//                    mAdView.setVisibility(View.GONE);
//                    break;
//                }
//            }
//        }
//    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration config= new AndroidApplicationConfiguration();
        config.useAccelerometer = false;
        config.useCompass = false;

        // Create the layout
        RelativeLayout layout = new RelativeLayout(this);

        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        // Create the libgdx View
        View gameView = initializeForView(new FindTheSpot(this), config);

        // Create and setup the AdMob view
        mAdView = new AdView(this);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.setAdSize(com.google.android.gms.ads.AdSize.SMART_BANNER);
        mAdView.setAdUnitId("ca-app-pub-5571713154558265/9272996632");
//        mAdView.setAdUnitId("ca-app-pub-3940256099942544/6300978111"); // test ads
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
                                  public void onAdLoaded() {
                                      mAdView.bringToFront();
                                  }
                              });

        // Add the libgdx view
        layout.addView(gameView);

        // Add the AdMob view
        RelativeLayout.LayoutParams adParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        layout.addView(mAdView, adParams);

        // Hook it all up
        setContentView(layout);
    }

    @Override
    public void showAds(final boolean show) {
//        handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
        AndroidLauncher.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(show) {
                    mAdView.setVisibility(View.VISIBLE);
                } else {
                    mAdView.setVisibility(View.GONE);
                }
            }
        });

    }
}
