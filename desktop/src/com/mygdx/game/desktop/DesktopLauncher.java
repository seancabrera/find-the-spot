package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cabrera.findthespot.FindTheSpot;
import com.cabrera.findthespot.IAdHandler;

public class DesktopLauncher implements IAdHandler {
    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "FindTheSpot";
//        config.width = 800;
//        config.height = 480;
        config.width = 480;
        config.height = 800;
        new LwjglApplication(new FindTheSpot(new DesktopLauncher()), config);
    }

    @Override
    public void showAds(boolean show) {

    }
}
